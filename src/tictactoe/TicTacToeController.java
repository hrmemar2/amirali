package tictactoe;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class TicTacToeController implements Initializable {


    @FXML
    Button b1;
    @FXML
    Button b2;
    @FXML
    Button b3;
    @FXML
    Button b4;
    @FXML
    Button b5;
    @FXML
    Button b6;
    @FXML
    Button b7;
    @FXML
    Button b8;
    @FXML
    Button b9;
    @FXML
    ChoiceBox choiceBox;
    @FXML
    TextField P1 , P2 ;

    @FXML
    GridPane gameBoard;

    private boolean playvsAI = false;
    private boolean isFirstPlayer = true;
    private boolean run = false ;
    public void buttonClickHandler(ActionEvent evt) {
        if(run){
        ArrayList<Button> buttons = new ArrayList<>();
        buttons.add(b1);
        buttons.add(b2);
        buttons.add(b3);
        buttons.add(b4);
        buttons.add(b5);
        buttons.add(b6);
        buttons.add(b7);
        buttons.add(b8);
        buttons.add(b9);
        Button clickedButton = (Button) evt.getTarget();
        String buttonLabel = clickedButton.getText();

        if ("".equals(buttonLabel) && isFirstPlayer && playvsAI) {
            clickedButton.setText("X");
            isFirstPlayer = false;
            if (!find3InARow()){
            for(; ; ){
                Random random = new Random();
                Integer i = random.nextInt()%9;
                if(i<0) i = -1 * i ;
                if(buttons.get(i).getText().equals("")){
                    buttons.get(i).setText("O");
                    isFirstPlayer = true;
                    break;
                }
            }}

        } else if ("".equals(buttonLabel) && isFirstPlayer) {
            clickedButton.setText("X");
            isFirstPlayer = false;
        } else if ("".equals(buttonLabel) && !isFirstPlayer) {
            clickedButton.setText("O");
            isFirstPlayer = true;
        }


        find3InARow();  // is there a winner?
    }}

    private boolean findstruct(Button x, Button y, Button z) {
        if ("" != x.getText() && x.getText() == y.getText()
                && y.getText() == z.getText()) {
            return true;
        } else return false;

    }

    private boolean find3InARow() {
        //Row 1
        if (findstruct(b1, b2, b3)) {
            highlightWinningCombo(b1, b2, b3);
            return true;
        }
        //Row 2
        if (findstruct(b4, b5, b6)) {
            highlightWinningCombo(b4, b5, b6);
            return true;
        }
        //Row 3
        if (findstruct(b7, b8, b9)) {
            highlightWinningCombo(b7, b8, b9);
            return true;
        }

        //Column 1
        if (findstruct(b1, b4, b7)) {
            highlightWinningCombo(b1, b4, b7);
            return true;
        }
        //Column 2
        if (findstruct(b2, b5, b8)) {
            highlightWinningCombo(b2, b5, b8);
            return true;
        }
        //Column 3
        if (findstruct(b3, b6, b9)) {
            highlightWinningCombo(b3, b6, b9);
            return true;
        }
        //Diagonal 1
        if (findstruct(b1, b5, b9)) {
            highlightWinningCombo(b1, b5, b9);
            return true;
        }
        //Diagonal 2
        if (findstruct(b3, b5, b7)) {
            highlightWinningCombo(b3, b5, b7);
            return true;
        }
        return false;
    }

    private void highlightWinningCombo(Button first, Button second, Button third) {
        first.getStyleClass().add("winning-button");
        second.getStyleClass().add("winning-button");
        third.getStyleClass().add("winning-button");

    }

    public void RunAction(ActionEvent actionEvent){
        String p1 = P1.getText();
        String p2 = P2.getText();
        String choice = choiceBox.getValue().toString();
        if ("PlayervsAI".equals(choice)) {
           playvsAI = true;
        }
        run = true;


    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        choiceBox.getItems().add("PlayervsAI");
        choiceBox.getItems().add("PlayervsPlayer");
        choiceBox.setValue("PlayervsPlayer");

    }
}